//  Quiz - Part 1 of the Activity
/*
1. What is the term given to unorganized code that's very hard to work with?
    - spaghetti code
2. How are object literals written in JS?
    - object literals are written using curly braces {}
3. What do you call the concept of organizing information and functionality to belong to an object?
    - object-oriented programming (OOP)
4. If studentOne has a method named enroll(), how would you invoke it?
    - studentOne.enroll();
5. True or False: Objects can have objects as properties.
    - true
6. What is the syntax in creating key-value pairs?
    const objLiteral = {
             key1: value1,
             key2: value2,
            ...
    };

7. True or False: A method can have no parameters and still work.
    - true
8. True or False: Arrays can have objects as elements.
    - true
9. True or False: Arrays are objects.
    - true
10. True or False: Objects can have arrays as properties.
    - true
*/
