// Function Coding - Part 2 of the Activity

//1. Translate the other students from our boilerplate code into their own respective objects.

//2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

//3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

//4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

let studentOne = {
  name: "John",
  email: "john@mail.com",
  grades: [89, 84, 78, 88],

  login() {
    console.log(`${this.email} has logged in`);
  },

  logout() {
    console.log(`${this.email} has logged out`);
  },

  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
  },

  computeGradeAverage() {
    const sum = this.grades.reduce((total, grade) => total + grade, 0);
    const average = sum / this.grades.length;
    return average;
  },

  willPass() {
    return this.computeGradeAverage() >= 85;
  },

  willPassWithHonors() {
    const average = this.computeGradeAverage();
    if (average >= 90) {
      return true;
    } else if (average >= 85 && average < 90) {
      return false;
    } else {
      return undefined;
    }
  },
};

let studentTwo = {
  name: "Joe",
  email: "joe@mail.com",
  grades: [78, 82, 79, 85],

  login() {
    console.log(`${this.email} has logged in`);
  },

  logout() {
    console.log(`${this.email} has logged out`);
  },

  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
  },

  computeGradeAverage() {
    const sum = this.grades.reduce((total, grade) => total + grade, 0);
    const average = sum / this.grades.length;
    return average;
  },

  willPass() {
    return this.computeGradeAverage() >= 85;
  },

  willPassWithHonors() {
    const average = this.computeGradeAverage();
    if (average >= 90) {
      return true;
    } else if (average >= 85 && average < 90) {
      return false;
    } else {
      return undefined;
    }
  },
};

let studentThree = {
  name: "Jane",
  email: "jane@mail.com",
  grades: [87, 89, 91, 93],

  login() {
    console.log(`${this.email} has logged in`);
  },

  logout() {
    console.log(`${this.email} has logged out`);
  },

  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
  },

  computeGradeAverage() {
    const sum = this.grades.reduce((total, grade) => total + grade, 0);
    const average = sum / this.grades.length;
    return average;
  },

  willPass() {
    return this.computeGradeAverage() >= 85;
  },

  willPassWithHonors() {
    const average = this.computeGradeAverage();
    if (average >= 90) {
      return true;
    } else if (average >= 85 && average < 90) {
      return false;
    } else {
      return undefined;
    }
  },
};

let studentFour = {
  name: "Jessie",
  email: "jessie@mail.com",
  grades: [91, 89, 92, 93],

  login() {
    console.log(`${this.email} has logged in`);
  },

  logout() {
    console.log(`${this.email} has logged out`);
  },

  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
  },

  computeGradeAverage() {
    const sum = this.grades.reduce((total, grade) => total + grade, 0);
    const average = sum / this.grades.length;
    return average;
  },

  willPass() {
    return this.computeGradeAverage() >= 85;
  },

  willPassWithHonors() {
    const average = this.computeGradeAverage();
    if (average >= 90) {
      return true;
    } else if (average >= 85 && average < 90) {
      return false;
    } else {
      return undefined;
    }
  },
};

//5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

//6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

//7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

//8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

//9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

let classOf1A = {
  students: [studentOne, studentTwo, studentThree, studentFour],

  countHonorStudents() {
    let count = 0;
    for (let i = 0; i < this.students.length; i++) {
      if (this.students[i].willPassWithHonors()) {
        count++;
      }
    }
    return count;
  },

  honorsPercentage() {
    const totalStudents = this.students.length;
    const honorStudents = this.countHonorStudents();
    const percentage = (honorStudents / totalStudents) * 100;
    return percentage;
  },

  retrieveHonorStudentInfo() {
    let honorStudentsInfo = [];
    for (let i = 0; i < this.students.length; i++) {
      if (this.students[i].willPassWithHonors()) {
        let honorStudent = {
          email: this.students[i].email,
          averageGrade: this.students[i].computeGradeAverage(),
        };
        honorStudentsInfo.push(honorStudent);
      }
    }
    return honorStudentsInfo;
  },

  sortHonorStudentsByGradeDesc() {
    let honorStudents = this.retrieveHonorStudentInfo();
    honorStudents.sort((a, b) => b.averageGrade - a.averageGrade);
    return honorStudents;
  },
};
